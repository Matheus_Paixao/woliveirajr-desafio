<?php
session_start();
require_once 'configs/routes.php';

$routes = new routes();

// Busca a Controladora
$routeData = $routes->getRouteData();
if (empty($routeData)) {
    header("Location: http://desafio.webjump.com.br/dashboard");
}

// Nome da controladora
$controllerName = $routeData["controller"];
// Método a ser executado
$methodName = $routeData["method"];
require_once "controller/" . $controllerName . ".php";
$class = $controllerName;
// Instanciando a Controller
$obj = new $class();
$obj->$methodName();