<?php

require_once 'configs/environment.php';

$dbServer   = environment::DB_SERVER; // IP do servidor (Desenvolvendo em localhost)
$dbName     = environment::DB_NAME; // Banco
$dbUsername = environment::DB_USERNAME; // Login do banco
$dbPassword = environment::DB_PASSWORD; // Senha do banco

//faz a conexão com o banco sem o database
$conn = new mysqli($dbServer, $dbUsername, $dbPassword);

//verifica se não houve algum error com a conexão
if ($conn->connect_error) {
    die("Conexão com o banco de dados falhou: " . $conn->connect_error);
}

//Verifica se o Parametro DBName está preennchido, se não retornar que é obrigatorio
if (empty($dbName)) {
    die("Nome do banco de dados é obrigatório.");
}

//variável com o comando SQL
$sql = "CREATE DATABASE IF NOT EXISTS " . $dbName;

//executa o comando e retornar a mensagem
if ($conn->query($sql) === true) {
    echo "Database criado com sucesso '$dbName' \n";
} else {
    echo "Error ao tentar criar database: " . $conn->error . " \n";
}

//fecha a conexão
$conn->close();
//faz a conexão com o database já criado
$conn = new mysqli($dbServer, $dbUsername, $dbPassword, $dbName);

//variável com o comando SQL
$sql = "CREATE TABLE if NOT EXISTS database_tables (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    table_execute varchar(255),
    created_at datetime);";

//executa o comando e retornar a mensagem
if ($conn->query($sql) === true) {
    echo "Table database_tables criado com sucesso '$dbName' \n";
} else {
    echo "Error ao tentar criar table: database_tables \n";
}

//fecha a conexão
$conn->close();