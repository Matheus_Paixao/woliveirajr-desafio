<?php

require_once 'configs/environment.php';

$dbServer   = environment::DB_SERVER; // IP do servidor (Desenvolvendo em localhost)
$dbName     = environment::DB_NAME; // Banco
$dbUsername = environment::DB_USERNAME; // Login do banco
$dbPassword = environment::DB_PASSWORD; // Senha do banco

//faz a conexão com o database já criado
$conn = new mysqli($dbServer, $dbUsername, $dbPassword, $dbName);

//verifica se não houve algum error com a conexão
if ($conn->connect_error) {
    die("Database connection failed: " . $conn->connect_error);
}

//variável com o comando SQL
$sql = "CREATE TABLE if NOT EXISTS products (
    id int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name varchar(255),
    SKU varchar(255),
    image_path varchar(255),
    price double NOT NULL,
    description varchar(255),
    quantity int,
    active_flag boolean);";

//executa o comando e retornar a mensagem
if ($conn->query($sql) === true) {
    echo "Table products created successfully '$dbName' \n";
} else {
    echo "Error trying to create table: products \n";
}

//fecha a conexão
$conn->close();