<?php

require_once 'configs/environment.php';

$dbServer   = environment::DB_SERVER; // IP do servidor (Desenvolvendo em localhost)
$dbName     = environment::DB_NAME; // Banco
$dbUsername = environment::DB_USERNAME; // Login do banco
$dbPassword = environment::DB_PASSWORD; // Senha do banco

//faz a conexão com o database já criado
$conn = new mysqli($dbServer, $dbUsername, $dbPassword, $dbName);

//verifica se não houve algum error com a conexão
if ($conn->connect_error) {
    die("Database connection failed: " . $conn->connect_error);
}

//variável com o comando SQL
$sql = "CREATE TABLE if NOT EXISTS logs (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    class_name varchar(250) NOT NULL,
    type varchar(250) NOT NULL,
    class_id int UNSIGNED NOT NULL,
    json_log JSON,
    created_at datetime NOT NULL);";

//executa o comando e retornar a mensagem
if ($conn->query($sql) === true) {
    echo "Table product_category created successfully '$dbName' \n";
} else {
    echo "Error trying to create table: product_category \n";
}

//fecha a conexão
$conn->close();