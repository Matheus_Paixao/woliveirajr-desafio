<?php
require_once 'model/Category.php';
require_once 'controller/controllerTrait.php';

/**
 * Class categoryController
 * @author Wilson Junior
 */
class categoryController
{
    use controllerTrait;
    /**
     * @var Category
     */
    private $category;

    /**
     * ClienteController constructor.
     */
    public function __construct()
    {
        $this->category = new Category();
    }

    public function index()
    {
        $categories = $this->category->listAll();

        $_REQUEST['categories'] = $categories;

        require_once 'assets/categories/categories.php';
    }

    public function create()
    {
        //        $categories = $this->category->listAll();
        //        $_REQUEST['categories'] = $categories;

        require_once 'assets/categories/create_category.php';
    }

    /**
     *
     */
    public function store()
    {
        $data = $this->getRequestData();

        $categorySave = $this->category->save($data);
        if ($categorySave) {
            $_SESSION['notice'] = "Sucesso!";
        } else {
            $_SESSION['notice'] = "Erro!";
        }
        header("Location: " . $this->urlOriginal . "/category?notice=" . $_SESSION['notice']);
    }

    public function delete()
    {
        $id = $_GET['id'];
        if (!empty($id)) {
            if ($this->category->remove($id)) {
                header("Location: " . $this->urlOriginal . "/category?notice=Sucesso!");
                $_SESSION["notice"] = "Sucesso!";
            } else {
                header("Location: " . $this->urlOriginal . "/category?notice=Error!");
                $_SESSION['notice'] = "Error!";
            }
        } else {
            header("Location: " . $this->urlOriginal . "/category?notice=Código obrigatório!.");
            $_SESSION['notice'] = "Código obrigatório!";
        }
    }

    public function active()
    {
        $id = $_GET['id'];
        if (!empty($id)) {
            if ($this->category->active($id)) {
                header("Location: " . $this->urlOriginal . "/category?notice=Sucesso!");
                $_SESSION['notice'] = "Sucesso!";
            } else {
                header("Location: " . $this->urlOriginal . "/category?notice=Error!");
                $_SESSION['notice'] = "Error!";
            }
        } else {
            header("Location: " . $this->urlOriginal . "/category?notice=Código obrigatório!.");
            $_SESSION['notice'] = "Código obrigatório!";
        }
    }

    public function edit()
    {
        $id = $_GET['id'];
        if (!empty($id)) {
            $category = $this->category->findById($id);
            if (!empty($category)) {
                $_REQUEST['category'] = $category;

                require_once 'assets/categories/edit_category.php';
            } else {
                header("Location: " . $this->urlOriginal . "/category?notice=Error!");
                $_SESSION['notice'] = "Error!";
            }
        } else {
            header("Location: " . $this->urlOriginal . "/category?notice=Código obrigatório!.");
            $_SESSION['notice'] = "Código obrigatório!";
        }
    }

    public function update()
    {
        $id   = $_POST['id'];
        $data = $this->getRequestData();

        $categorySave = $this->category->update($data, $id);
        if ($categorySave) {
            $_SESSION['notice'] = "Sucesso!";
        } else {
            $_SESSION['notice'] = "Erro!";
        }
        header("Location: " . $this->urlOriginal . "/category?notice=" . $_SESSION['notice']);
    }
}