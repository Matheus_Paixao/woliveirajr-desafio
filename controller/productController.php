<?php
require_once 'model/Product.php';
require_once 'model/Category.php';
require_once 'model/ProductCategory.php';
require_once 'model/CsvImports.php';
require_once 'controller/controllerTrait.php';
require_once 'model/modelTrait.php';

/**
 * Class productController
 * @author Wilson Junior
 */
class productController
{
    use controllerTrait;
    /**
     * @var Product
     */
    private $product;
    /**
     * @var Category
     */
    private $category;
    /**
     * @var ProductCategory
     */
    private $productCategory;
    /**
     * @var CsvImports
     */
    private $csvImports;
    /**
     * @var mixed
     */
    private $urlOriginal;
    private $path;

    /**
     * productController constructor.
     */
    public function __construct()
    {
        $this->product         = new Product();
        $this->category        = new Category();
        $this->productCategory = new ProductCategory();
        $this->csvImports      = new CsvImports();
        $this->urlOriginal     = $_SERVER["HTTP_ORIGIN"];
        $this->path            = __DIR__;
        $this->path            = str_replace('controller', '', $this->path);
    }

    public function index()
    {
        $page = $_GET["page"];
        if (!$page) {
            $page = 1;
        }
        $products = $this->product->listAll(false, $page, 10);
        $this->product->closeDBConnection();
        $pages = $products["pages"];
        if (!empty($pages)) {
            if (strpos($pages, ".") !== false) {
                $pages = (int) $pages + 1;
            }
            $_REQUEST['pages'] = $pages;
        }
        unset($products["pages"]);
        $_REQUEST['products'] = $products;

        require_once 'assets/products/produtcs.php';
    }

    public function create()
    {
        $categories = $this->category->listAll(true);
        $this->product->closeDBConnection();
        $_REQUEST['categories'] = $categories;

        require_once 'assets/products/create_product.php';
    }

    public function edit()
    {
        $id = $_GET['id'];
        if (!empty($id)) {
            $product    = $this->product->findById($id);
            $categories = $this->category->listAll(true);
            $this->product->closeDBConnection();
            $_REQUEST['categories'] = $categories;
            if (!empty($product)) {
                $_REQUEST['product'] = $product;

                require_once 'assets/products/edit_product.php';
            } else {
                header("Location: " . $this->urlOriginal . "/product?notice=Error!");
                $_SESSION['notice'] = "Error!";
            }
        } else {
            header("Location: " . $this->urlOriginal . "/product?notice=Código obrigatório!.");
            $_SESSION['notice'] = "Código obrigatório!";
        }

        require_once 'assets/products/edit_product.php';
    }

    public function saveCSVFile()
    {
        $dateNow = new DateTime();
        $dateNow = $dateNow->format('Y-m-d H:i:s');
        $files   = $_FILES;
        if (isset($files["csv"]) && $files['csv']['error'] == 0) {
            $fileTmp   = $files['csv']['tmp_name'];
            $name      = $files['csv']['name'];
            $extension = pathinfo($name, PATHINFO_EXTENSION);
            if (strstr('.csv', $extension)) {
                $newName = uniqid(time()) . '.' . $extension;
                $folder  = 'files/csvfiles/' . $newName;
                if (@move_uploaded_file($fileTmp, $folder)) {
                    $csvImportsData = array_filter(
                        [
                            "file_path"   => $this->path . $folder,
                            "executed_at" => null,
                            "created_at"  => $dateNow,
                            "updated_at"  => $dateNow,
                        ]
                    );

                    $csvImports = $this->csvImports->save($csvImportsData);
                    //        $this->closeDatabaseConnection();
                    if ($csvImports) {
                        $_SESSION['notice'] = "Sucesso!";
                    } else {
                        $_SESSION['notice'] = "Erro!";
                    }

                    header("Location: " . $this->urlOriginal . "/product?notice=" . $_SESSION['notice']);

                    return;
                }
            }
            header("Location: " . $this->urlOriginal . "/product?notice=Extensão válida é 'CSV'!");

            return;
        }
        header("Location: " . $this->urlOriginal . "/product?notice=Arquivo necessário para importação!");

        return;
    }

    public function importCSVFile($csvData)
    {
        foreach ($csvData as $value) {
            $productSave = $this->product->save($value);
            if (empty($productSave)) {
                continue;
            }
            $lastInsertedProduct = $this->product->getLastInserted();
            if ($lastInsertedProduct) {
                $categories = $this->category->findByName($value["categories"]);
                if (empty($categories) && !empty($value["categories"][0])) {
                    if (is_array($value["categories"])) {
                        foreach ($value["categories"] as $category) {
                            $data = [
                                "name"        => $category,
                                'active_flag' => 1,
                                "code"        => random_int(1000000, 9999999),
                            ];
                            $this->category->save($data);
                        }
                    }
                    $categories = $this->category->findByName($value["categories"]);
                }
                foreach ($categories as $category) {
                    $productCategoryData = [
                        "product_id"  => $lastInsertedProduct[0]["id"],
                        "category_id" => $category["id"],
                    ];
                    $this->productCategory->save($productCategoryData);
                }
            }
        }

        return true;
    }

    public function store()
    {
        $data          = $this->getRequestData();
        $data['price'] = str_replace(',', '.', $data['price']);
        $files         = $_FILES;
        $image_path    = null;
        if (isset($files["image"]) && $files['image']['error'] == 0) {
            $fileTmp   = $files['image']['tmp_name'];
            $name      = $files['image']['name'];
            $extension = pathinfo($name, PATHINFO_EXTENSION);
            if (strstr('.jpg;.jpeg;.gif;.png', $extension)) {
                $newName = $data["name"] . uniqid(time()) . '.' . $extension;
                $folder  = 'assets/images/product/' . $newName;
                if (@move_uploaded_file($fileTmp, $folder)) {
                    $image_path = $folder;
                }
            }
        }
        if (!empty($image_path)) {
            $data["image_path"] = $this->urlOriginal . '/' . $image_path;
        }

        $productSave         = $this->product->save($data);
        $lastInsertedProduct = $this->product->getLastInserted();
        $categories          = $this->category->findByIds($data["categories"]);
        if ($lastInsertedProduct) {
            foreach ($categories as $category) {
                $productCategoryData = [
                    "product_id"  => $lastInsertedProduct[0]["id"],
                    "category_id" => $category["id"],
                ];
                $this->productCategory->save($productCategoryData);
            }
        }
        //        $this->closeDatabaseConnection();
        if ($productSave) {
            $_SESSION['notice'] = "Sucesso!";
        } else {
            $_SESSION['notice'] = "Erro!";
        }

        header("Location: " . $this->urlOriginal . "/product?notice=" . $_SESSION['notice']);
    }

    /**
     * @return void
     * @description Encerra conexão com o banco
     */
    public function closeDatabaseConnection()
    {
        modelTrait::closeConnection();
    }

    public function delete()
    {
        $id = $_GET['id'];
        if (!empty($id)) {
            if ($this->product->remove($id)) {
                header("Location: " . $this->urlOriginal . "/product?notice=Sucesso!");
                $_SESSION["notice"] = "Sucesso!";
            } else {
                header("Location: " . $this->urlOriginal . "/product?notice=Error!");
                $_SESSION['notice'] = "Error!";
            }
        } else {
            header("Location: " . $this->urlOriginal . "/product?notice=Código obrigatório!.");
            $_SESSION['notice'] = "Código obrigatório!";
        }
    }

    public function active()
    {
        $id = $_GET['id'];
        if (!empty($id)) {
            if ($this->product->active($id)) {
                header("Location: " . $this->urlOriginal . "/product?notice=Sucesso!");
                $_SESSION['notice'] = "Sucesso!";
            } else {
                header("Location: " . $this->urlOriginal . "/product?notice=Error!");
                $_SESSION['notice'] = "Error!";
            }
        } else {
            header("Location: " . $this->urlOriginal . "/product?notice=Código obrigatório!.");
            $_SESSION['notice'] = "Código obrigatório!";
        }
    }

    public function update()
    {
        $data          = $this->getRequestData();
        $data['price'] = str_replace('.', '', $data['price']);
        $data['price'] = str_replace(',', '.', $data['price']);
        $files         = $_FILES;
        $image_path    = null;
        if (isset($files["image"]) && $files['image']['error'] == 0) {
            $fileTmp   = $files['image']['tmp_name'];
            $name      = $files['image']['name'];
            $extension = pathinfo($name, PATHINFO_EXTENSION);
            if (strstr('.jpg;.jpeg;.gif;.png', $extension)) {
                $newName = $data["name"] . uniqid(time()) . '.' . $extension;
                $folder  = 'assets/images/product/' . $newName;
                if (@move_uploaded_file($fileTmp, $folder)) {
                    $image_path = $folder;
                }
            }
        }
        if (!empty($image_path)) {
            $data["image_path"] = $this->urlOriginal . '/' . $image_path;
        }

        $this->productCategory->removeAllProduct($data['id']);
        $categories = $this->category->findByIds($data["categories"]);
        foreach ($categories as $category) {
            $productCategoryData = [
                "product_id"  => $data["id"],
                "category_id" => $category["id"],
            ];
            $this->productCategory->save($productCategoryData);
        }
        $productUpdate = $this->product->update($data, $data['id']);
        if ($productUpdate) {
            $_SESSION['notice'] = "Sucesso!";
        } else {
            $_SESSION['notice'] = "Erro!";
        }

        header("Location: " . $this->urlOriginal . "/product?notice=" . $_SESSION['notice']);
    }
}