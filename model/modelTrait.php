<?php

require_once 'configs/environment.php';

/**
 * Trait modelTrait
 */
trait modelTrait
{
    /**
     * @var null
     */
    private $databaseConnection = null;

    /**
     * @param $sql
     * @return bool|string
     */
    private function executeQuery($sql)
    {
        $this->databaseConnect();
        $stmt         = mysqli_prepare($this->databaseConnection, $sql);
        $queryExecute = mysqli_stmt_execute($stmt);
        if ($stmt && $queryExecute) {
            mysqli_stmt_close($stmt);

            return $queryExecute;
        } else {

            return "Something went wrong. Please try again later.";
        }

        return false;
    }

    /**
     * @param $sql
     * @return bool|string
     */
    private function getQueryResult($sql)
    {
        $this->databaseConnect();
        $stmt         = mysqli_prepare($this->databaseConnection, $sql);
        $queryExecute = mysqli_stmt_execute($stmt);
        if ($stmt && $queryExecute) {
            mysqli_stmt_close($stmt);

            return true;
        } else {

            return "Something went wrong. Please try again later.";
        }

        return false;
    }

    /**
     * @param $data
     * @return array
     */
    private function getQuerydataByRequest($data)
    {
        $arrayKeys = array_keys($data);
        $keys      = implode(',', $arrayKeys);
        $values    = [];
        foreach ($data as $key => $value) {
            if (in_array($key, ["image", "image_path"])) {
                $values[] = "'" . $value . "'";
            } else {
                $values[] = "'" . str_replace("'", '', $value) . "'";
            }
        }
        $values = implode(',', $values);
        $values = str_replace("''", "','", $values);

        return [
            "keys"   => $keys,
            "values" => $values,
        ];
    }

    /**
     * @return void
     */
    private function databaseConnect()
    {
        try {
            $dbServer   = environment::DB_SERVER; // IP do servidor (Desenvolvendo em localhost)
            $dbName     = environment::DB_NAME; // Banco
            $dbUsername = environment::DB_USERNAME; // Login do banco
            $dbPassword = environment::DB_PASSWORD; // Senha do banco

            $this->databaseConnection = mysqli_connect($dbServer, $dbUsername, $dbPassword, $dbName);
            // Checando conexão
            if ($this->databaseConnection === false || $this->databaseConnection === null) {
                die("ERROR: Could not connect. " . mysqli_connect_error());
            }
        } catch (Exception $ex) {
            var_dump($ex);
        }
    }

    /**
     * @return void
     */
    protected function closeConnection()
    {
        mysqli_close($this->databaseConnection);
    }

    /**
     * @param $data
     * @return string
     */
    private function getQuerydataToUpdate($data)
    {
        $sql = '';
        foreach ($data as $key => $value) {
            if (is_string($value)) {
                $value = "'" . $value . "'";
            }
            if (empty($sql)) {
                $sql = $sql . ' ' . $key . '=' . $value;
            } else {
                $sql = $sql . ',' . ' ' . $key . '=' . $value;
            }
        }

        return $sql;
    }
}
