<?php

require_once 'model/modelTrait.php';

/**
 * Class ProductCategory
 * @author Wilson Junior
 */
class ProductCategory
{
    use modelTrait;
    /**
     * @var string
     */
    private $name = "product_category";
    /**
     * @var array
     * @description Colunas que irão trazer do banco
     */
    private $fillable = [
        "id",
        "product_id",
        "category_id",
    ];

    /**
     * ProductCategory constructor.
     */
    public function __construct()
    {
        $this->databaseConnect();
    }

    /**
     * @return void
     * @description Close database connection
     */
    public function closeDBConnection()
    {
        $this->closeConnection();
    }

    /**
     * ...
     * getters e setters
     * ...
     */

    /**
     * @param $data
     * @return bool
     */
    public function save($data)
    {
        $data   = $this->getQuerydataByRequest($data);
        $keys   = $data["keys"];
        $values = $data["values"];

        $sql = "INSERT INTO $this->name ($keys) VALUES ($values)";
        $this->executeQuery($sql);

        return true;
    }
    public
    function removeAllProduct($productId)
    {
        $result = $this->getQueryResult("DELETE FROM $this->name WHERE product_id = $productId;");
        if ($result === true) {
            return true;
        } else {
            return false;
        }

        return false;
    }

    /**
     *
     */
    public
    function listAll()
    {
        try {
            // Attempt select query execution
            $sql            = "SELECT * FROM $this->name";
            $databaseResult = mysqli_query($this->databaseConnection, $sql);
            $result         = [];
            while ($row = mysqli_fetch_array($databaseResult)) {
                $aux = [];
                foreach ($this->fillable as $fillable) {
                    $aux[$fillable] = $row[$fillable];
                }
                $result[] = $aux;
            }

            return $result;
        } catch
        (Exception $ex) {
            var_dump($ex->getMessage());
        }
    }
    /**
     * ...
     * outros métodos de abstração de banco
     * ...
     */
}

?>