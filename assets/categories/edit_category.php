<?php
$category = $_REQUEST['category'];
require_once 'assets/required/bootstrap.php';
?>
<style>
    <?php include 'assets/css/style.css';?>
</style>
<div id="mySidenav" class="sidenav text-center">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="/category">Categorias</a>
    <a href="/product">Produtos</a>
    <img src='../assets/images/go-logo.png'>
</div>
<header>
    <title>Webjump | Backend Test | Dashboard</title>
    <div class="go-menu">
        <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
        <a href="dashboard.html" class="link-logo">
            <img src="../assets/images/go-logo.png" alt="Welcome" width="69" height="430"/></a>
    </div>
</header>
<body>
<h1 class='text-center'>Desafio</h1>
<div class='card mt-1'>
    <div class='card-body'>
        <h1 class="title new-item">Editar Categoria</h1>
        <form action="../category/update" method="POST">
            <div class='row'>
                <div class='form-group col-md-6'>
                    <label for='name'>Nome</label>
                    <input type='text' class='form-control' name='name' id='name' value='<?php echo $category['name'] ?>'>
                </div>
                <div class='form-group col-md-6'>
                    <label for='code'>Código</label>
                    <input type='text' class='form-control' name='code' id='code' value='<?php echo $category['code'] ?>'>
                </div>
            </div>
            <input type='hidden' hidden name='id' id='id' value='<?php echo $category['id'] ?>'>
            <button type='submit' class='btn btn-success '>Salvar</button>
        </form>
    </div>
</div>
</body>
<script>
    <?php include 'assets/required/scripts.js'?>
</script>
<footer>
    <div class="footer-image">
        <img src="../assets/images/go-jumpers.png" width="119" height="26" alt="Go Jumpers"/>
    </div>
    <div class="email-content">
        <span>wjunior013@gmail.com</span>
    </div>
</footer>